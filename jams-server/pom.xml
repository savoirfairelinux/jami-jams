<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>jams3-parent</artifactId>
        <groupId>net.jami</groupId>
        <version>${revision}</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>jams-server</artifactId>
    <dependencies>
        <dependency>
            <groupId>net.jami</groupId>
            <artifactId>cryptoengine</artifactId>
            <version>${revision}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.tomcat.embed</groupId>
            <artifactId>tomcat-embed-core</artifactId>
            <version>${tomcat.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.tomcat.embed</groupId>
            <artifactId>tomcat-embed-jasper</artifactId>
            <version>${tomcat.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.tomcat</groupId>
            <artifactId>tomcat-annotations-api</artifactId>
            <version>${tomcat.version}</version>
        </dependency>
        <dependency>
            <groupId>net.jami</groupId>
            <artifactId>datastore</artifactId>
            <version>${revision}</version>
        </dependency>
        <dependency>
            <groupId>net.jami</groupId>
            <artifactId>jams-common</artifactId>
            <version>${revision}</version>
        </dependency>
        <dependency>
            <groupId>net.jami</groupId>
            <artifactId>jami-dht</artifactId>
            <version>${revision}</version>
        </dependency>
        <dependency>
            <groupId>com.nimbusds</groupId>
            <artifactId>nimbus-jose-jwt</artifactId>
            <version>${nimbus.jwt.version}</version>
        </dependency>
        <dependency>
            <groupId>org.ow2.asm</groupId>
            <artifactId>asm</artifactId>
            <version>${asm.version}</version>
        </dependency>
        <dependency>
            <groupId>net.jami</groupId>
            <artifactId>authentication-module</artifactId>
            <version>${revision}</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>net.jami</groupId>
            <artifactId>jami-nameserver</artifactId>
            <version>${revision}</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.zeromq</groupId>
            <artifactId>jeromq</artifactId>
            <version>0.5.2</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpcore</artifactId>
            <version>4.4.12</version>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
            <version>4.5.14</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven.compiler.version}</version>
                <executions>
                    <execution>
                        <id>Compile With Annotation Processing</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                        <configuration>
                            <source>11</source>
                            <target>11</target>
                            <annotationProcessorPaths>
                                <path>
                                    <groupId>net.jami</groupId>
                                    <artifactId>jams-common</artifactId>
                                    <version>${revision}</version>
                                </path>
                            </annotationProcessorPaths>
                            <annotationProcessors>
                                <annotationProcessor>
                                    lombok.launch.AnnotationProcessorHider$AnnotationProcessor
                                </annotationProcessor>
                                <annotationProcessor>
                                    net.jami.jams.common.annotations.ScopedServletProcessor
                                </annotationProcessor>
                            </annotationProcessors>
                            <compilerArgs>
                                <arg>-AmoduleDir=${pom.basedir}</arg>
                                <arg>-AparentDir=${project.parent.basedir}</arg>
                            </compilerArgs>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-resources-plugin</artifactId>
                <version>${maven.resources.version}</version>
                <executions>
                    <execution>
                        <id>copy-resource-one</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${pom.basedir}/target/classes</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>${pom.basedir}/target/generated-sources/</directory>
                                    <filtering>false</filtering>
                                </resource>
                            </resources>
                            <overwrite>true</overwrite>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>${maven.shade.version}</version>
                <executions>
                    <!-- Run shade goal on package phase -->
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <outputFile>../jams/${project.artifactId}.jar</outputFile>
                            <filters>
                                <filter>
                                    <artifact>*:*</artifact>
                                    <excludes>
                                        <exclude>module-info.class</exclude>
                                        <!-- <exclude>META-INF.versions.9.module-info</exclude> -->
                                        <exclude>META-INF/DEPENDENCIES</exclude>
                                        <exclude>META-INF/LICENSE</exclude>
                                        <exclude>META-INF/LICENSE.txt</exclude>
                                        <exclude>META-INF/MANIFEST.MF</exclude>
                                        <exclude>META-INF/NOTICE</exclude>
                                        <exclude>META-INF/NOTICE.txt</exclude>
                                        <exclude>META-INF/services/javax.annotation.processing.Processor</exclude>
                                        <exclude>META-INF/*.SF</exclude>
                                        <exclude>META-INF/*.DSA</exclude>
                                        <exclude>META-INF/*.RSA</exclude>
                                    </excludes>
                                </filter>
                            </filters>
                            <transformers>
                                <!-- add Main-Class to manifest file -->
                                <transformer
                                        implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                    <manifestEntries>
                                        <Main-Class>net.jami.jams.server.Server</Main-Class>
                                        <Implementation-Title>${project.artifactId}</Implementation-Title>
                                        <Implementation-Version>${project.version}</Implementation-Version>
                                        <Class-Path>.</Class-Path>
                                    </manifestEntries>
                                </transformer>
                                <transformer
                                        implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer"/>
                            </transformers>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>