<style type='text/css'>
img{
height:400px;
}
</style>

# Client Guide

Depending on your operating system, we have included the tutorial on how to connect to the management server from the Windows, MacOS, Android and iOS clients.


For the purposes of this tutorial, we assume that

1. The server and the device attempting to connect are either
	1. On the same network
	2. The server is publicly accessible to the outside world
2. You have a valid username/password pair to connect to the server

## Connect from a Linux device

Open Jami, go to the login page. Click on "Advanced":

![alt text][linuxstep1]

[linuxstep1]: https://static.savoirfairelinux.com/img/jams/client/linux/linux-step1.png "Step1"

Select the option **"Connect to a JAMS server"** which will lead you to the following screen:

![alt text][linuxstep2]

[linuxstep2]: https://static.savoirfairelinux.com/img/jams/client/linux/linux-step2.png "Step2"

The **Jami Account Management Server URL** in this case would be the DNS address of your server and the username and password which correspond to your account. If you have configured the server with an LDAP/AD backend, it would be your LDAP/AD username and password.

## Connect from a Windows device

Open Jami, go to the login page. Click on "Advanced":

![alt text][windowsstep1]

[windowsstep1]: https://static.savoirfairelinux.com/img/jams/client/windows/windows-step1.png "Windows step 1"

Select the option **"Connect to a JAMS server"** which will lead you to the following screen:

![alt text][windowsstep2]

[windowsstep2]: https://static.savoirfairelinux.com/img/jams/client/windows/windows-step2.png "Windows step 2"

The **Jami Account Management Server URL** in this case would be the DNS address of your server and the username and password which correspond to your account. If you have configured the server with an LDAP/AD backend, it would be your LDAP/AD username and password.

## Connect from a macOS device

Open Jami, go to the login page. Click on "Advanced":

![alt text][macosstep1]

[macosstep1]: https://static.savoirfairelinux.com/img/jams/client/macos/macos-step1.png "macOS step 1"


Select the option **"Connect to account manager"** which will lead you to the following screen:

![alt text][macosstep2]

[macosstep2]: https://static.savoirfairelinux.com/img/jams/client/macos/macos-step2.png "macOS step 2"

The **Jami Account Management Server URL** in this case would be the DNS address of your server and the username and password which correspond to your account. If you have configured the server with an LDAP/AD backend, it would be your LDAP/AD username and password.

## Connect from an Android device

Open Jami, go to the login page.

![alt text][androidstep1]

[androidstep1]: https://static.savoirfairelinux.com/img/jams/client/android/android-step1.png "Android step 1"

Select the option **"Connect to management server"** which will lead you to the following screen:

![alt text][androidstep2]

[androidstep2]: https://static.savoirfairelinux.com/img/jams/client/android/android-step2.png "Android step 2"

The server in this case would be the DNS address of your server and the username and password which correspond to your account. If you have configured the server with an LDAP/AD backend, it would be your LDAP/AD username and password.

## Connect from an iOS device
Open Jami, go to the login page.

![alt text][iosstep1]

[iosstep1]: https://static.savoirfairelinux.com/img/jams/client/ios/ios-step1.png "iOS step 1"

Select the option **"Connect to account manager"** which will lead you to the following screen:

![alt text][iosstep2]

[iosstep2]: https://static.savoirfairelinux.com/img/jams/client/ios/ios-step2.png "iOS step 2"

The server in this case would be the DNS address of your server and the username and password which correspond to your account. If you have configured the server with an LDAP/AD backend, it would be your LDAP/AD username and password.